using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float currentHealth = 100f;

    public float playerCooldown = 1;
    public float damage = 1;
    public bool block = false;
    public int currentItem = -1;

    public GameObject itemDrop;
    public GameObject[] items = new GameObject[9];

    public bool _enemyInRange = false;
    public bool _canAttack = true;
    public bool _itemPick = false;

    public GameObject enemy;
    public Animator anim;

    private GameObject _item;
    [SerializeField]
    private Image _BarHP;
    [SerializeField]
    private Text _BarHPtxt;
    [SerializeField]
    private ItemUI _itmUI;
    private float _maxHealth;

    private void Awake()
    {
        _maxHealth = currentHealth;
        _BarHPtxt.text = $"{currentHealth}";
        anim = this.GetComponent<Animator>();
    }
    private void Update()
    {
        if (!this.GetComponent<PlayerControll>().isDeath)
        {

            if (_maxHealth > currentHealth)
            {
                _BarHP.fillAmount = currentHealth / _maxHealth;
                _BarHPtxt.text = $"{currentHealth}";
            }
            if (Input.GetKey(KeyCode.E) && _itemPick)
            {
                int x = PickItem();

                if (x >= 0 && _item != null)
                {
                    items[x] = _item;
                    for (int i = 0; i < _itmUI.ssitm.Length; i++)
                    {
                        if (_item.name == _itmUI.ssitm[i].name)
                        {
                            _itmUI.itms[x].GetComponent<Image>().sprite = _itmUI.ssitm[i];
                            _itmUI.itms[x].GetComponent<Image>().color = new Color(255, 255, 255, 255);
                        }
                    }
                    _item.SetActive(false);
                    _itemPick = false;
                }
            }
            if (Input.GetKey(KeyCode.Q) && items[currentItem] != null)
            {
                items[currentItem].transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z + 1);
                items[currentItem].SetActive(true);
                items[currentItem] = null;
                _itmUI.itms[currentItem].GetComponent<Image>().sprite = null;
                _itmUI.itms[currentItem].GetComponent<Image>().color = new Color(255, 255, 255, 25);
                damage = 1;
                playerCooldown = 1;
            }

            if (Input.GetMouseButton(0))
            {
                if (_enemyInRange && _canAttack && enemy != null && enemy.CompareTag("Enemy"))
                {
                    enemy.GetComponent<Enemy>().curentHealth -= damage;
                    enemy.GetComponent<Enemy>().animE.SetBool("TakeDamage", true);
                }
                else if(enemy != null)
                    enemy.GetComponent<Enemy>().animE.SetBool("TakeDamage", false);

                anim.SetBool("Atack", true);
                StartCoroutine(AttackCooldown());
            }
            else
                anim.SetBool("Atack", false);


            if (Input.GetMouseButton(1))
                block = true;
            else
                block = false;

            if (Input.GetKey(KeyCode.Alpha1))
                DefaultDamage(0);
            if (Input.GetKey(KeyCode.Alpha2))
                DefaultDamage(1);
            if (Input.GetKey(KeyCode.Alpha3))
                DefaultDamage(2);
            if (Input.GetKey(KeyCode.Alpha4))
                DefaultDamage(3);
            if (Input.GetKey(KeyCode.Alpha5))
                DefaultDamage(4);
            if (Input.GetKey(KeyCode.Alpha6))
                DefaultDamage(5);
            if (Input.GetKey(KeyCode.Alpha7))
                DefaultDamage(6);
            if (Input.GetKey(KeyCode.Alpha8))
                DefaultDamage(7);
            if (Input.GetKey(KeyCode.Alpha9))
                DefaultDamage(8);
        }
        else if (this.GetComponent<PlayerControll>().isDeath)
        {
            anim.SetBool("Death", true);
        }
    }

    void DefaultDamage(int x)
    {
        if (items[x] != null)
        {
            damage = items[x].GetComponent<Weapon>().weaponAtack;
            playerCooldown = items[x].GetComponent<Weapon>().weaponCooldown;
            currentItem = x;
        }
        else if (items[x] == null)
        {
            damage = 1;
            playerCooldown = 1;
        }
    }

    int PickItem()
    {
        for (int i = 0; i < items.Length; i++)
        {
            if (items[i] == null)
                return i;

        }
        return -1;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            enemy = other.gameObject;
            _enemyInRange = true;
        }
        if (other.gameObject.CompareTag("Item"))
        {
            _item = other.gameObject;
            _itemPick = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            _enemyInRange = false;
        }
        if (other.gameObject.CompareTag("Item"))
        {
            _itemPick = false;
        }
    }
    IEnumerator AttackCooldown()
    {
        _canAttack = false;
        yield return new WaitForSeconds(playerCooldown);
        _canAttack = true;
    }

}

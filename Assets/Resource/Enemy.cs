using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{

    public float curentHealth = 50;

    public float enemyCooldown = 1;
    public float damage = 1;
    public Animator animE;

    private float _maxHealth;
    private bool _playerInRange = false;
    private bool _canAttack = true;
    private GameObject _player;
    [SerializeField]
    private Image _bar;

    private void Awake()
    {
        _maxHealth = curentHealth;
        animE = this.GetComponent<Animator>();
    }

    private void Update()
    {
        if (_maxHealth > curentHealth)
            _bar.fillAmount = curentHealth / _maxHealth;

        if (_playerInRange && _canAttack && !_player.GetComponent<Player>().block)
        {
            animE.SetBool("Atack", true);
            _player.GetComponent<Player>().currentHealth -= damage;
            _player.GetComponent<Player>().anim.SetBool("TakeDamage", true);
            StartCoroutine(AttackCooldown());
        }
        else if (_player != null)
        {
            animE.SetBool("Atack", false);
            _player.GetComponent<Player>().anim.SetBool("TakeDamage", false);
        }

        if (curentHealth <= 0)
        {
            animE.SetBool("Death", true);
            Destroy(this.gameObject);
            _player.GetComponent<Player>().enemy = null;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _player = other.gameObject;
            _playerInRange = true;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerInRange = false;
        }
    }
    IEnumerator AttackCooldown()
    {
        _canAttack = false;
        yield return new WaitForSeconds(enemyCooldown);
        _canAttack = true;
    }
}

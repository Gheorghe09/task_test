using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class EnemyMovePlayer : MonoBehaviour
{
    Transform playerTransform;
    NavMeshAgent myNavmesh;

    public float checkRate = 0.01f;

    private Animator _animE;

    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Player").activeInHierarchy)
            playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        myNavmesh = gameObject.GetComponent<NavMeshAgent>();
        _animE = this.GetComponent<Animator>();

    }

    void Update()
    {
        if (Vector3.Distance(playerTransform.position,this.transform.position) < checkRate &&
            Vector3.Distance(playerTransform.position, this.transform.position) > 2.5)
        {
            _animE.SetBool("Forward", true);
            FollowPlayer();
        }
        else
        {
            _animE.SetBool("Forward", false);
        }
    }

    void FollowPlayer()
    {
        myNavmesh.transform.LookAt(playerTransform);
        myNavmesh.destination = playerTransform.position;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, checkRate);
    }
}

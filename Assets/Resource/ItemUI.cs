using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemUI : MonoBehaviour
{
    public GameObject itm;

    public Sprite[] ssitm = new Sprite[4];
    public GameObject[] itms = new GameObject[9];
    void Awake()
    {
        for (int i = 0; i < itms.Length; i++)
        {
            //itm.GetComponentInChildren<Image>().sprite = null;
            //itm.GetComponentInChildren<Image>().color = new Color(255,255,255,25);
            itm.GetComponentInChildren<Text>().text = $"{i + 1}";
            itm.name = $"{i + 1}";
            Instantiate(itm,this.transform);
            itms[i] = this.transform.GetChild(i).gameObject;
        }
    }

}
